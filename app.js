//Part 1
let number = 7;
let _name = 'Eugene';
if (number => 0 && number <= 9) console.log(true);
else console.log(false);

if (_name === 'Eugene') console.log(`Name: ${_name}`);
else console.log('New user');

//Part 2
const Func = (num) => {
    if (typeof num !== 'number') {
        throw new Error('Input must be a number');
    }
    if (num === 10) return true;
    return false;
}

try {
    Func('qwerty');
} catch (err) {
    console.log('Error:', err.message);
}